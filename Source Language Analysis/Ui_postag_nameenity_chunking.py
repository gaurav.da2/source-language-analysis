from flask import Flask, jsonify, render_template, request
import sys
import os
import nltk
from nltk.tag import tnt
from nltk.corpus import indian
from nltk.tree import Tree
from nltk.tokenize import sent_tokenize, word_tokenize
app = Flask(__name__)

@app.route('/Pos_tags',methods = ['POST', 'GET'])
def POS_tags():
    Pos_tag=""
    result=[]
    s=[]
    sentence = request.args.get('sentence', 0, type=str)
    options = request.args.get('options', 0, type=str)
    sentence=sentence.replace(",","")
    sentence=sentence.replace("(","")
    sentence=sentence.replace(")","")
    sentence=sentence.replace("|","")
    sentence=sentence.replace(";","")
    sentence=sentence.replace("?","")
    sentence=sentence.replace("!","")
    sentence=sentence.replace("—","")
    sentence=sentence.replace("‐","")
    sentence=sentence.replace("”","")
    sentence=sentence.replace(":-","")
    s=word_tokenize(sentence)
    file = open("out_hindi.txt","w")
    for word in s:
    	file.write("\n"+word)
    file.close()
    if options=="POS_tags":
        content=[]
        open('output.txt', 'w').close()
        os.system("./tnt finaltokenze_1 out_hindi.txt>>output.txt")
        text = open('output.txt', 'r+')
        outp = open('test_out.txt','w')
        for line in text:
            if not line.lstrip().startswith('%%'):
                content.append(line)
                outp.write(line)
                print(line)
        content=content[1:]        
        text.close()
        outp.close()
    if options=="Named_Entity":
        content=[]
        open('output.txt', 'w').close()
        os.system("./tnt finaltokenze_1 out_hindi.txt>>output.txt")
        text = open('output.txt', 'r+')
        outp = open('test_out.txt','w')
        for line in text:
            if not line.lstrip().startswith('%%'):
                content.append(line)
                outp.write(line)
                print(line)
        content=content[1:]
        text.close()
        outp.close()
        open('name.txt', 'w').close()
        os.system("crf_test -m model_crf test_out.txt>>name.txt")
        text1 = open('name.txt', 'r+')
        content = text1.readlines()
        content = content[:-1]
        text1.close()
    )
    if options=="Chunking":
        def hindi_model():
            train_data = indian.tagged_sents('hindi.pos')
            tnt_pos_tagger = tnt.TnT()
            tnt_pos_tagger.train(train_data)
            return tnt_pos_tagger
        def get_keywords(pos):
            grammar = r"""NP:{<NN.*>}"""
            chunkParser = nltk.RegexpParser(grammar)
            chunked = chunkParser.parse(pos)
            continuous_chunk = set()
            current_chunk = []
            for i in chunked:
                if type(i) == Tree:
                    current_chunk.append(" ".join([token for token, pos in i.leaves()]))
                elif current_chunk:
                    named_entity = " ".join(current_chunk)
                    if named_entity not in continuous_chunk:
                        continuous_chunk.add(named_entity)
                        current_chunk = []
                    else:
                        continue
            return (continuous_chunk)
        model = hindi_model()
        new_tagged = model.tag(s)
        print(new_tagged)
        print()
        print("====KEYWORDS===")
        print(get_keywords(new_tagged))
        L=get_keywords(new_tagged)
        y=",".join(str(x) for x in L)
        content=y
    return jsonify(result=content)
@app.route('/')
def index():
    return render_template('index.html')
if __name__ == '__main__':
    app.run(debug=True)   	

